SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `ag_schools` ;
CREATE SCHEMA IF NOT EXISTS `ag_schools` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `ag_schools` ;

-- -----------------------------------------------------
-- Table `ag_schools`.`school`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ag_schools`.`school` ;

CREATE TABLE IF NOT EXISTS `ag_schools`.`school` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


INSERT INTO `ag_schools`.`school` (`id`, `name`) VALUES 
(1, 'UChicago'),
(2, 'Stanford'),
(3, 'Harvard'),
(4, 'MIT');

