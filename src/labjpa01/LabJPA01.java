/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package labjpa01;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author ag
 */
public class LabJPA01 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
                
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("labJPA01PU");
        EntityManager em = emf.createEntityManager();
        
        em.getTransaction().begin();
        School school = new School();
        school.setName("Princeton");
        em.persist(school);
        em.getTransaction().commit();
        
        em.close();
        emf.close();
        
        
    }
    
}
